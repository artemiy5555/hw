package HW_1.Функции;

import HW_1.УсловныеOператоры.Operator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FunctionTest {
    @Test
    void week() {
        assertEquals("Воскресенье", Function.week(2));
    }
//    @Test
//    void captcha() {
//        assertEquals("Воскресенье", Function.captcha(1234));
//    }
    @Test
    void toNum() {
        assertEquals(200, Lessons3.toNum("двести",0));
    }
    @Test
    void twoPoint() {
        assertEquals(2.8284271247461903, Lessons4.twoPoint(1,2,3,4));
    }
}
