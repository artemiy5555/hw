package HW_1.ОдномерныеМассивы;


import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;


public class ArrayTest {
    @Test
    void minElement() {
        assertEquals(1, Array.minElement(new int[]{1,2,3,4}));
    }
    @Test
    void maxElement() {
        assertEquals(4, Array.maxElement(new int[]{1,2,3,4}));
    }
    @Test
    void minIndex() {
        assertEquals(1, Array.minIndex(new int[]{1,2,3,4}));
    }
    @Test
    void maxIndex() {
        assertEquals(4, Array.maxIndex(new int[]{1,2,3,4}));
    }
    @Test
    void sumElementsIndex() {
        assertEquals(4, Array.sumElementsIndex(new int[]{1,2,3,4}));
    }
    @Test
    void reversArr() {
        assertEquals(new int[]{4,3,2,1}, Array.reversArr(new int[]{1,2,3,4}));
    }
    @Test
    void sumNecElements() {
        assertEquals(new int[]{3,4,1,2}, Array.sumNecElements(new int[]{1,2,3,4}));
    }
    @Test
    void revers() {
        assertEquals(new int[]{3,4,1,2}, Array.revers(new int[]{1,2,3,4}));
    }
    @Test
    void bubble() {
        assertEquals(new int[]{3,4,1,2}, Array.bubble(new int[]{1,2,3,4}));
    }
    @Test
    void select() {
        assertEquals(new int[]{3,4,1,2}, Array.select(new int[]{1,2,3,4}));
    }
    @Test
    void insert() {
        assertEquals(new int[]{3,4,1,2}, Array.insert(new int[]{1,2,3,4}));
    }

}
