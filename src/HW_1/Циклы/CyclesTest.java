package HW_1.Циклы;

import HW_1.Функции.Function;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CyclesTest {
    @Test
    void sum() {
        assertEquals(new int[]{2450, 49}, Cycles.sum());
    }
    @Test
    void simpleNumber() {
        assertTrue( Cycles.simpleNumber(5));
    }
    @Test
    void sqrtOne() {
        assertEquals(3 ,Cycles.sqrtOne(12));
    }
    @Test
    void sqrtTwo() {
        assertEquals(4 ,Cycles.sqrtTwo(12));
    }
    @Test
    void factorial() {
        assertEquals(120 ,Cycles.factorial(5));
    }
    @Test
    void sumNumbers() {
        assertEquals(10 ,Cycles.sumNumbers(55));
    }
    @Test
    void reversNumbers() {
        assertEquals(21 ,Cycles.reversNumbers(12));
    }
}
