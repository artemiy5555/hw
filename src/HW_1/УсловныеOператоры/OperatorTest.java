package HW_1.УсловныеOператоры;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
public class OperatorTest {
    @Test
    void oneL() {
        assertEquals(6, Operator.oneL(2,3));
    }
    @Test
    void quarter() {
        assertEquals(1, Operator.quarter(2,3));
    }
    @Test
    void positive() {
        assertEquals(9, Operator.positive(2,3,4));
    }
    @Test
    void max() {
        assertEquals(27, Operator.max(2,3,4));
    }
    @Test
    void mark() {
        assertEquals("C", Operator.mark(60));
    }
}
