package Randomaizer;

public class Randomaizer {

    public static void main(String[] args) {
        randomMun();
        tenRandomMun();
        tenRandomMunZeroTen();
        tenRandomMunTTwoTFive();
        tenRandomMunMinusTenTen();
        diapazonRandomMun();
    }

    static void randomMun(){
        System.out.println("Вывести на консоль случайное число.");
        System.out.println(Math.random());
    }
    static void tenRandomMun(){
        System.out.println("Вывести на консоль 10 случайных чисел.");
        for(int i=0;i<10;i++)
            randomMun();
    }
    static void tenRandomMunZeroTen(){
        System.out.println("Вывести на консоль 10 случайных чисел, каждое в диапазоне от 0 до 10.");
        for(int i=0;i<10;i++)
            System.out.println(0 + (int) (Math.random() * 10));
    }
    static void tenRandomMunTTwoTFive(){
        System.out.println("Вывести на консоль 10 случайных чисел, каждое в диапазоне от 20 до 50.");
        for(int i=0;i<10;i++)
            System.out.println(20 + (int) (Math.random() * 50));
    }
    static void tenRandomMunMinusTenTen(){
        System.out.println("Вывести на консоль 10 случайных чисел, каждое в диапазоне от -10 до 10.");
        for(int i=0;i<10;i++)
            System.out.println(-10 + (int) (Math.random() * 10));
    }
    static void diapazonRandomMun(){
        System.out.println("Вывести на консоль случайное количество (в диапазоне от 3 до 15)\n" +
                "случайных чисел, каждое в диапазоне от -10 до 35.");
        int forNum = 3 + (int) (Math.random() * 15);

        for(int i=0;i<forNum;i++)
            System.out.println(-10 + (int) (Math.random() * 35  ));
    }
}
